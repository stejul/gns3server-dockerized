FROM ubuntu:latest

MAINTAINER Stefan

ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 

RUN apt-get update &&  \
    apt upgrade --yes && \
    apt-get install --yes make && \
    apt-get install --yes build-essential git libpthread-stubs0-dev libpcap-dev qemu-kvm libvirt-bin virtinst bridge-utils cpu-checker cmake libelf-dev libpcap0.8-dev && \
    apt-get install --yes locales python3-setuptools python3-aiohttp python3-psutil python3-jsonschema

RUN locale-gen en_US.UTF-8

RUN cd ~ && \
    git clone https://github.com/GNS3/ubridge.git && \
    cd ubridge && \
    make && \
    make install

RUN cd ~ && \
    git clone git://github.com/GNS3/dynamips.git && \
    cd dynamips && \
    mkdir build && \
    cd build && \
    cmake .. -DDYNAMIPS_CODE=stable && \
    make install

#RUN apt install --yes apt-transport-https ca-certificates curl software-properties-common && \
#    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
#    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \\
#    apt install --yes docker-ce

RUN cd ~ && \
    git clone https://github.com/GNS3/gns3-server.git && \
    cd gns3-server/ && \
    python3 setup.py install

RUN cd ~ && \
    apt-get install --yes docker.io && \
    systemctl enable docker

RUN adduser gns3 && \
    adduser gns3 kvm && \
    adduser gns3 docker && \
    cd ~/gns3-server/conf && \
    rm gns3_server.conf

#COPY gns3_server.conf ~/gns3-server/conf/

RUN cp ~/gns3-server/init/gns3.service.systemd /lib/systemd/system/gns3.service

RUN chmod +x ~/gns3-server/scripts/remote-install.sh

EXPOSE 3080

CMD cd ~/gns3-server/ && \
    python3 -m gns3server --local